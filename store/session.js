export const state = () => ({
    token: undefined
})

export const mutations = {
    login(state, token) {
        state.token = token
    },
    logout(state) {
        state.token = undefined
    }
}